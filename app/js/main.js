$(document).ready(function(){
  setPageType();
});

function setPageType(){
  if ($('#root_cars').length) {
    $('a#cars').addClass('active');
    // [app_id, token]
    return ['2f84296a10aa5a90','92d5b1bbc58e77a87fa1c9c60b6b3889']
  } else if($('#root_travel').length) {
    $('a#travel').addClass('active')
    return ['5f13508601112399','2b3f4c0a96ac1a168100c2685b287b4e']
  } else if($('#root_luxury').length) {
    $('a#luxury').addClass('active')
    return ['3788ce239419bdda', 'ae49a4b3750acdea2ae0da67e3ef7377']
  }
}

function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  var year = a.getFullYear();
  var month = a.getMonth() + 1;
  var date = a.getDate();
  
  if(month < 10) {
    month = "0" + month;
  }
  if(date < 10) {
    date = "0" + date;
  }
  var time = ' ' + date + '.' + month + '.' + year
  return time;
}

function trimUsername(username){
  if (username.length > 13){
    return $.trim(username).substring(0, 13) + '...';
  } else {
    return username
  }
}

var ReactTransitionGroup = React.addons.CSSTransitionGroup;
var Modal = React.createClass({

  getInitialState: function(){
    return{
      // token: 'd87af123cf66a759ffd86183eeb3a198',
      token: setPageType()[1],
      image: null,
      caption: null,
      username: null,
      video: null
    }
  },
  getOneMedia: function(){
    this.loading('on')
    $.ajax({
      type: 'GET',
      url: 'http://instahub.kloudi.co/api/v1/media/one/' + this.props.image_id,
      // url: 'http://localhost:8080/api/v1/media/one/' + this.props.image_id,
      headers: {"Authorization":"Token token=" + this.state.token}
    }).done(function(result) {
      if (this.isMounted()) {
        this.setState({
          image: result.instagram_images.standard_resolution.url,
          caption: result.caption,
          username: result.instagram_username        
        });
      }
      if (result.instagram_videos){
        this.setState({
          video: result.instagram_videos.standard_resolution.url
        });
      }    
    }.bind(this));
  },

  loading:function(action){  
    if(action=="on"){
      $('.object-big').width(this.props.object_width/(this.props.object_height/600))
      $('.object-big').height(this.props.object_height)
    } else if (action=="off") {
      $('.object-big').css('height', 'auto');
      $('.object-big').css('width', 'auto');
    }
  },

  show:function(){
    $('.img-responsive').removeClass('img-hide')
    $('.img-responsive').hide();
    $('.img-responsive').fadeIn(300)
  },

  componentDidMount: function(){        
    this.getOneMedia();
    document.addEventListener("keydown", function (e) {
      if (e.keyCode == 27) {
          this.props.handleHideModal();
      }
    }.bind(this));
  },
  render: function(){
    if (this.props.object_type == 'image'){
      var object = <img src={this.state.image} onLoad = {this.show} className = 'img-responsive object-big img-hide'></img>
      var load_button = <a href={this.state.image + '?fdl=1'} download target='_blank'><i className="fa fa-download"/></a>
    } else if (this.props.object_type == 'video') {
      var object = <div className='embed-responsive object-big'><video className='embed-responsive-item' src={this.state.video} autoPlay></video></div>
      var load_button = <a href={this.state.video} download target="_blank"><i className="fa fa-download"/></a>
    }

    var caption = $.trim(this.state.caption).substring(0, 80).split(" ").slice(0, -1).join(" ") + "...";
    
    return (
      <div>
        <div onClick={this.props.handleHideModal} className = 'image-modal-backdrop'/>
          <ReactTransitionGroup transitionName="modal-show-animation" transitionAppear={true} transitionAppearTimeout={200}>
            <div className='image-modal'>
              <div className='row'>       
                <div className='col-xs-12 object-container'>
                  {object}
                </div>
              </div>  
              <div className='row description'>
                <div className='col-xs-5'>
                  <h5><a href={'https://instagram.com/' + this.state.username}><i className="fa fa-instagram"/>{this.state.username}</a></h5>
                </div>
                <div className='col-xs-2 text-center'>
                  {load_button}
                </div>
                <div className='col-xs-5 text-right'>
                  <p>{caption}</p>
                </div>
              </div>
            </div>
          </ReactTransitionGroup>
      </div>
      )
  },
});

var MediaStrip = React.createClass({
  getInitialState:function(){
    window.addEventListener("scroll", this.handleScroll);
    console.log(setPageType()[1])
    return{
      page_number: 1,
      // loadingFlag:false,   //to avoid multiple fetch request if user is keep scrolling
      // app_id: 'ac6f2d0dece2e154',
      // token: 'd87af123cf66a759ffd86183eeb3a198',
      app_id: setPageType()[0],
      token: setPageType()[1],
      photo_count: 12,
      random: 0,
      pImage: [],
      showModal: false,
      part_show: true,
      image_id: 0,
      // banner_place: [Math.floor((Math.random() * (11 - 2) + 2)),Math.floor((Math.random() * (23 - 14) + 14)),Math.floor((Math.random() * (35 - 26) + 26)),Math.floor((Math.random() * (47 - 38) + 38)),Math.floor((Math.random() * (59 - 50) + 50))]
      banner_place: []
    }
  },
  handleScroll:function(e){
    if($(window).scrollTop() == $(document).height() - $(window).height()){  //user reached at bottom
      if(!this.state.loadingFlag){  //to avoid multiple request
          this.setState({
            loadingFlag:true,
          });
        this.getMedia();
      }
    }
  },

  loading:function(action){
    if(action=="on"){
      $('#loader').removeClass('loader-witout-animation').addClass('loader')
    } else if (action=="off") {
      $('#loader').removeClass('loader').addClass('loader-witout-animation')      
    }
  },

  getMedia:function(){
    this.loading('on')
    var next_page = this.state.page_number + 1
    $.ajax({
      type: 'GET',
      url: 'http://instahub.kloudi.co/api/v1/media/all/' + this.state.app_id + '/' + this.state.page_number + '/' + this.state.photo_count + "/" + this.state.random,
      // url: 'http://localhost:8080/api/v1/media/all/' + this.state.app_id + '/' + this.state.page_number + '/' + this.state.photo_count + "/" + this.state.random,
      headers: {"Authorization":"Token token=" + this.state.token}
    }).done(function(result) { 
      var images_with_banners = []
      var images_without_banners =  this.state.pImage.concat(result)
      var rand = this.state.banner_place[this.state.page_number - 1] - 1
      images_without_banners.map(function (image, index) {
        if (index == rand){
          images_with_banners.push('banner')
          images_with_banners.push(image)          
        } else {
          images_with_banners.push(image);
        }
      });
      if (this.isMounted()) {
        this.setState({
          pImage: images_with_banners,
          loadingFlag:false,
          page_number: next_page,
        });
      }
    this.loading('off')
    }.bind(this));
  },  
  componentWillMount: function(){
    this.getMedia();
  },
  render: function(){  
    this.loading('on');
    return <Media key={this.state.page_number} images={this.state.pImage}/>;
  },

});

var Media = React.createClass({
  getInitialState:function(){  
    return{
      showModal: false,
      image_id: 0,
      object_type: null,
      object_width: null,
      object_height: null      
    }
  },
  handleHideModal: function(e){
    this.setState({showModal: false, image_id: 0, object_type: null, object_width: null, object_height: null});    
  },
  handleShowModal: function(e){
    this.setState({showModal: true, image_id: e[0], object_type: e[1], object_width: e[2], object_height: e[3]});
  },

    render:function(){
      this.state.showModal ? $('#header').addClass('blur') && $('.image-container').addClass('blur') : $('#header').removeClass('blur') && $('.image-container').removeClass('blur')
      var modal =  this.state.showModal ? <Modal image_id={this.state.image_id} handleHideModal={this.handleHideModal} object_type={this.state.object_type} object_width={this.state.object_width} object_height={this.state.object_height}/> : null
      var Images = this.props.images.map(function (image, index) {        
        if (image == 'banner'){
          var block = <div><Banner/><div className='ads'>РЕКЛАМА</div></div>
        } else {
          var time = timeConverter(image.created_time)
          var username = trimUsername(image.instagram_username)
          var video_play = (image.type != 'image' ? <div className = 'video-play'></div> : '')          
          var block = <div className = 'image-container' onClick={this.handleShowModal.bind(this, [image._id.$oid, image.type, image.instagram_images.standard_resolution.width, image.instagram_images.standard_resolution.height])} >{video_play}<div style={{backgroundImage: 'url(' + image.instagram_images.low_resolution.url + ')'}} className = "image"></div><div className = 'image-info row'><div className = 'col-xs-8'><a href={'https://instagram.com/' + image.instagram_username}><i className="fa fa-instagram"/>{username}</a></div><div className = 'col-xs-4 date text-right'>{time}</div></div></div>
        }
   
        return (            
          <div key={index} className = 'col-xs-6 col-md-4 col-lg-3 image-col'>                           
            {block}
          </div>
        );
        }, this);
      return (
        <div>{modal}{Images}</div>
      );
    }
});


ReactDOM.render(<MediaStrip/>,document.getElementById('root'));


// var Banner = React.createClass({

//   getInitialState:function(){
//     return{
//       show: false,
//       id: 0,
//       banners: ['3247650198', '1667695393']
//     }
//   },

//   componentWillMount:function(){
//     if (this.state.show){
//       (adsbygoogle = window.adsbygoogle || []).push({});
//     }
//   },

//   shouldComponentUpdate:function(){
//     return this.state.show
//   },

//   componentDidMount:function(){
//     var next_id = this.state.id + 1;
//     this.setState({id: next_id});
//     (adsbygoogle = window.adsbygoogle || []).push({});
//     this.setState({show: true})
//   },

//   render:function(){
//     var banner_id = this.state.banners[this.state.id]
//     return <ins className="adsbygoogle" style={{display:'block', height: '250px', width: '250px'}} data-ad-client="ca-pub-9393438190116381" data-ad-slot={banner_id}></ins>
//   },
// });
