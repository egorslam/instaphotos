require 'rubygems'
# require 'sinatra'
require 'sinatra/base'
require "sinatra/reloader"
require 'sinatra/assetpack'
require 'haml'

class InstaPhotos < Sinatra::Base
  register Sinatra::AssetPack 

  configure :development do
    register Sinatra::Reloader
  end

  assets do
    serve '/js',     from: 'app/js'        # Default
    serve '/css',    from: 'app/css'       # Default
    # serve '/images', from: 'app/images'    # Default

    # The second parameter defines where the compressed version will be served.
    # (Note: that parameter is optional, AssetPack will figure it out.)
    js :application, [
      '/js/react-with-addons.min.js',
      '/js/react-dom.min.js',
      '/js/jquery-2.2.0.min.js',
    ]

    css :application, [
      '/css/*.css'
    ]

    js_compression  :jsmin    # :jsmin | :yui | :closure | :uglify
    css_compression :simple   # :simple | :sass | :yui | :sqwish
  end

  get '/?:type?' do
    @type = params[:type] || 'cars'
    haml :index
  end

  run! if app_file == $0
end

